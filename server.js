//Import các thư viện cần dùng
var express = require("express");
var { graphqlHTTP } = require("express-graphql");
var { buildSchema } = require("graphql");
require("dotenv").config();
// Xây dựng một Schema, sử dụng ngôn ngữ Schema GraphQL
var port = process.env.PORT || 4000;
const cors = require("cors");
const { mongoConnection } = require("./db");

//Tạo server với express
var app = express();

var AuthSchema = require("./schema/authInfomation");

console.log(AuthSchema);
// Cho phép `cors` thiết lập HTTP response header: Access-Control-Allow-Origin: *
app.use(cors());
//Khai báo API graphql
app.use(
  "/graphql",
  graphqlHTTP({
    schema: AuthSchema,
    graphiql: true, //sử dụng công cụ GraphiQL để đưa ra các query GraphQL theo cách thủ công
  })
);

// Khởi tạo server tại port ${port}
app.listen(port);
console.log(`Running a GraphQL API server at http://localhost:${port}/graphql`);

const { UserModel } = require("./models/user");

var newUser = new UserModel({
  name: "New User",
  email: "hoanghiep20179706@gmail.com",
  password: "thaovtt1234",
});

newUser.save((err) => {
  if (err) {
    console.log("Error When Save User To MongoDB: " + err);
  }
});
