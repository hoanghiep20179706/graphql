import gql from "graphql-tag";
import express from "express";
import { ApolloServer, makeExecutableSchema } from "apollo-server-express";
import { GraphQLDate, GraphQLDateTime, GraphQLTime } from "graphql-iso-date";
import { applyMiddleware } from "graphql-middleware";
import { UserModel } from "./models/user";
import { PostModel } from "./models/post";
import { CommentModel } from "./models/comment";
import { MongoObjectId } from "./scalas/MongoObjectId";
import { PubSub } from "graphql-subscriptions";
import { DateTimeScala } from "./scalas/DateTime";
const pubsub = new PubSub();

require("dotenv").config();

const { mongoConnection } = require("./db");

const port = process.env.PORT || 4000;
const chats = [];

// Define APIs using GraphQL SDL
const typeDefs = gql`
  scalar Date
  scalar Time
  scalar DateTime
  scalar MongoObjectId

  # A library has a branch and books
  type Library {
    branch: String!
    books: [Book!]
  }

  type Book {
    title: String
    author: Author
  }

  # A User has a posts and comments
  type User {
    id: ID
    name: String
    email: String
    password: String
    posts: [Post]
  }

  type Post {
    id: ID
    name: String
    content: String
    userId: String
    comments: [Comment]
    user: User
  }

  type Comment {
    id: ID
    content: String
    userId: String
    postId: String
    timestamp: DateTime
  }

  # An author has a name
  type Author {
    name: String!
  }

  type Query {
    users: [User]
    user(id: ID!): User
    posts: [Post]
    post(id: ID!): Post
    comments: [Comment]
    comment(id: ID!): Comment
    currentNumber: Int
    chats: [Chat]
  }

  type Mutation {
    sayHello(name: String!): String!
    addUser(name: String!, email: String, password: String!): User!
    addPost(name: String, content: String, userId: ID!): Post!
    addComment(
      content: String!
      userId: ID!
      postId: ID!
      timestamp: String
    ): Comment!
    updateComment(
      id: ID!
      content: String!
      userId: ID!
      postId: ID!
      timestamp: String
    ): Comment!
    sendMessage(from: String!, message: String!): Chat
    deleteComment(id: ID!): Comment!
  }

  type Subscription {
    addUser: User
    addComment: Comment
    messageSent: Chat
  }

  type Chat {
    id: Int!
    from: String!
    message: String!
  }
`;

// Define resolvers map for API definitions in SDL
const resolvers = {
  DateTime: DateTimeScala,

  Subscription: {
    addUser: {
      subscribe: (root, args, { pubsub }) => {
        return pubsub.asyncIterator(["CREATE_USER"]);
      },
    },
    addComment: {
      subscribe: (root, args, { pubsub }) => {
        return pubsub.asyncIterator(["ADD_COMMENT"]);
      },
    },
    messageSent: {
      subscribe: (root, args, { pubsub }) => {
        return pubsub.asyncIterator("CHAT");
      },
    },
  },

  Query: {
    chats(root, args, context) {
      return chats;
    },
    currentNumber() {
      return currentNumber;
    },
    users: (obj, args, context, info) => {
      return UserModel.find({});
    },
    user: (obj, args, context, info) => {
      return UserModel.findById(args.id);
    },
    posts: (obj, args, context, info) => {
      return PostModel.find({});
    },
    post: (obj, args, context, info) => {
      return PostModel.findById(args.id);
    },
    comments: (obj, args, context, info) => {
      return CommentModel.find({});
    },
    comment: (obj, args, context, info) => {
      return CommentModel.findById(args.id);
    },
  },

  User: {
    posts: (user) => {
      return PostModel.find({ userId: user.id });
    },
  },

  Post: {
    comments: (post) => {
      return CommentModel.find({ postId: post.id });
    },
    user: (post) => {
      return UserModel.findById(post.userId);
    },
  },

  Mutation: {
    sendMessage(root, { from, message }, { pubsub }) {
      const chat = { id: chats.length + 1, from, message };
      chats.push(chat);
      pubsub.publish("CHAT", { messageSent: chat });

      return chat;
    },
    addUser: function (obj, args, { pubsub }, info) {
      let newUser = new UserModel({
        name: args.name,
        email: args.email,
        password: args.password,
      });
      return newUser.save().then((data) => {
        console.log(
          "PUBLISH EVENT: CREATE_USER - " + JSON.stringify({ addUser: data })
        );
        pubsub.publish("ADD_USER", { addUser: data });
        return data;
      });
    },
    addPost: (obj, args, context, info) => {
      let newPost = new PostModel({
        name: args.name,
        content: args.content,
        userId: args.userId,
      });
      return newPost.save();
    },
    addComment: function (obj, args, context, info) {
      let newComment = new CommentModel({
        content: args.content,
        userId: args.userId,
        postId: args.postId,
        timestamp: args.timestamp,
      });
      let dbReturn = newComment.save().then((data) => {
        console.log(
          "PUBLISH EVENT: ADD_COMMENT - " + JSON.stringify({ addComment: data })
        );
        pubsub.publish("ADD_COMMENT", { addUser: data });
        return data;
      });
      console.log("DB RETURN - " + JSON.stringify(dbReturn));
      return dbReturn;
    },
    updateComment: function (obj, args, context, info) {
      let newComment = new CommentModel({
        content: args.content,
        userId: args.userId,
        postId: args.postId,
        timestamp: args.timestamp,
      });
      return CommentModel.findOneAndUpdate({ _id: args.id }, args, {
        new: true,
      }).then((data) => {
        console.log(
          "PUBLISH EVENT: UPDATE_COMMENT - " +
            JSON.stringify({ updateComment: data })
        );
        return data;
      });
    },
    deleteComment: async (obj, args, context, info) => {
      let deletedComment = await CommentModel.findByIdAndDelete({
        _id: args.id,
      }).then((comment) => {
        console.log(
          "PUBLISH EVENT: DELETED_COMMENT - " +
            JSON.stringify({ updateComment: comment })
        );
        return comment;
      });
      return deletedComment;
    },
  },
};

// Configure express
const app = express();

// Build GraphQL schema based on SDL definitions and resolvers maps
const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
  subscriptions: {
    path: "/subscriptions",
    onConnect: (connectionParams, webSocket, context) => {
      console.log("Client connected");
    },
    onDisconnect: (webSocket, context) => {
      console.log("Client disconnected");
    },
  },
  context: ({ req }) => {
    ({ pubsub });
  },
});

var permissions = require("./authenticate/shield");

console.log(permissions);
/* const schemaWithMiddleware = applyMiddleware(
  schema,
  shield(permissions, { allowExternalErrors: true })
); */
// Build Apollo server
const apolloServer = new ApolloServer({
  typeDefs,
  resolvers,
  subscriptions: {
    onConnect: (connectionParams, webSocket, context) => {
      console.log("Client connected");
    },
    onDisconnect: (webSocket, context) => {
      console.log("Client disconnected");
    },
  },
  context: ({ req }) => {
    console.log(JSON.stringify(pubsub));
    return { pubsub: `${pubsub}` };
  },
});

apolloServer.applyMiddleware({ app });

// Run server
const httpServer = app.listen({ port }, () => {
  console.log(
    `🚀Server ready at http://localhost:${port}${apolloServer.graphqlPath}`
  );
  console.log(
    `Subscriptions ready at ws://localhost:${port}${apolloServer.subscriptionsPath}`
  );
});

// add socket server to apolloServer
apolloServer.installSubscriptionHandlers(httpServer);
