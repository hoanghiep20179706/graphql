var mongoose = require("mongoose");
mongoose.connect(process.env.MONGO_URL, {
  useNewUrlParser: true,
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const mongoConnection = mongoose.connection;
mongoConnection.on("error", function (err) {
  if (err)
    console.log(
      "Không kết nối được tới mongodb; db url = " +
        process.env.MONGO_URL +
        "; err: " +
        err
    );
});

//Bắt sự kiện open
mongoConnection.once("open", function () {
  console.log(
    "Kết nối tới mongodb: " + process.env.MONGO_URL + " thành công !"
  );
});

module.exports = { mongoConnection };
