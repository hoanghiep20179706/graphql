var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var BalanceSchema = new Schema({
  value: String,
  userId: String,
});

var BalanceModel = mongoose.model("balances", BalanceSchema);

module.exports = { BalanceModel };
