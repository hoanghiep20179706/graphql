var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var CommentSchema = new Schema({
  content: String,
  userId: String,
  postId: String,
  timestamp: { type: Date, default: Date.now },
});

var CommentModel = mongoose.model("comments", CommentSchema);

module.exports = { CommentModel };
