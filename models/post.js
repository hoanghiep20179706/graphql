var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var PostSchema = new Schema({
  name: String,
  content: String,
  userId: String,
});

var PostModel = mongoose.model("posts", PostSchema);

module.exports = { PostModel };
