const graphql = require("graphql");
const { UserModel } = require("../models/user");
const { BalanceModel } = require("../models/balance");
const { PostModel } = require("../models/post");
const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLSchema,
  GraphQLID,
  GraphQLList,
  GraphQLNonNull,
} = graphql;
var _ = require("lodash");
var users = [
  {
    id: "1",
    name: "Hoang Hiep",
    email: "hoanghiep20179706@gmail.com",
    password: "thaovtt1234",
  },
  {
    id: "2",
    name: "thaovtt",
    email: "thaovtt@gmail.com",
    password: "thaovtt1234",
  },
];

var balances = [
  {
    id: "1",
    value: "69000",
    userId: "1",
  },
  {
    id: "2",
    value: "1000",
    userId: "1",
  },
];

const PostType = new GraphQLObjectType({
  name: "post",
  fields: () => ({
    id: { type: GraphQLID },
    content: { type: GraphQLString },
    timestamp: { type: GraphQLString },
    userId: { type: GraphQLID },
    users: {
      type: new GraphQLList(UserType),
      resolve(parent, args) {
        return UserModel.find({ _id: parent.id });
      },
    },
  }),
});

const UserType = new GraphQLObjectType({
  name: "user",
  fields: () => ({
    id: { type: GraphQLID },
    name: { type: GraphQLString },
    email: { type: GraphQLString },
    password: { type: GraphQLString },
    balances: {
      type: new GraphQLList(BalanceType),
      resolve(parent, args) {
        return BalanceModel.find({ userId: parent.id });
      },
    },
  }),
});

const BalanceType = new GraphQLObjectType({
  name: "balance",
  fields: () => ({
    id: { type: GraphQLID },
    value: { type: GraphQLString },
    userId: { type: GraphQLID },
    users: {
      type: new GraphQLList(UserType),
      resolve(parent, args) {
        return UserModel.find({ _id: parent.userId });
      },
    },
  }),
});

const RootQuery = new GraphQLObjectType({
  name: "RootQuery",
  fields: {
    user: {
      type: UserType,
      args: { id: { type: GraphQLID } },
      resolve(parent, args) {
        return UserModel.findById(args.id);
      },
    },
    users: {
      type: new GraphQLList(UserType),
      resolve(parent, args) {
        return UserModel.find({});
      },
    },
    balance: {
      type: BalanceType,
      args: { id: { type: GraphQLID } },
      resolve(parent, args) {
        return BalanceModel.findById(args.id);
      },
    },
    balances: {
      type: new GraphQLList(BalanceType),
      resolve(parent, args) {
        return BalanceModel.find({});
      },
    },
    post: {
      type: PostType,
      args: { id: { type: GraphQLID } },
      resolve(parent, args) {
        return PostModel.findById(args.id);
      },
    },
    posts: {
      type: new GraphQLList(PostType),
      resolve(parent, args) {
        return PostModel.find({});
      },
    },
    users: async () => {
      try {
        const users = await UserModel.find({});
        return users.map((user) => ({
          ...user._doc,
          posts: posts.bind(this, author._doc.books),
        }));
      } catch (err) {
        throw err;
      }
    },
  },
});

const Mutation = new GraphQLObjectType({
  name: "Mutation",
  fields: () => ({
    addUser: {
      type: UserType,
      args: {
        name: { type: new GraphQLNonNull(GraphQLString) },
        email: { type: GraphQLString },
        password: { type: new GraphQLNonNull(GraphQLString) },
      },
      resolve(parent, args) {
        let newUser = new UserModel({
          name: args.name,
          email: args.email,
          password: args.password,
        });
        return newUser.save();
      },
    },
    addBalance: {
      type: BalanceType,
      args: {
        value: { type: GraphQLString },
        userId: { type: GraphQLID },
      },
      resolve(parent, args) {
        let newBalance = new BalanceModel({
          value: args.value,
          userId: args.userId,
        });
        return newBalance.save();
      },
    },
    addPost: {
      type: PostType,
      args: {
        content: { type: GraphQLString },
        userId: { type: GraphQLID },
      },
      resolve(parent, args) {
        let newPost = new PostModel({
          content: args.content,
          userId: args.userId,
        });
        return newBalance.save();
      },
    },
  }),
});

module.exports = new GraphQLSchema({ query: RootQuery, mutation: Mutation });
